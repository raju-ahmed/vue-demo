import apiClient from '@/utils/api'

export function getAllUsers () {
  return apiClient.get('/users')
}

export function getUserDetail (id) {
  return apiClient.get('/users/' + id)
}
